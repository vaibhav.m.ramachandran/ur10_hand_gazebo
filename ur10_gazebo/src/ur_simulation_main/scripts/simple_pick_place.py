#!/usr/bin/env python3

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import actionlib
import geometry_msgs

def simple_pick_place():
    #init moveit commander
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('simple_pick_place',
                  anonymous=True)

    #instantiate a moveit commander object
    robot1_group = moveit_commander.MoveGroupCommander("robot1")

    #action clients to execute trajectory
    robot1_client = actionlib.SimpleActionClient('execute_trajectory',moveit_msgs.msg.ExecuteTrajectoryAction)
    robot1_client.wait_for_server()
    rospy.loginfo('Execute Trajectory server is available for robot1')

    robot1_client = move_bot_to_pose("R1Home",robot1_group,robot1_client)
    input("Press Enter to continue...")
    robot1_client = move_bot_to_pose("R1Pick",robot1_group,robot1_client)
    input("Press Enter to continue...")
    robot1_client = move_bot_to_pose("R1Place",robot1_group,robot1_client)

    #shut down moveit_commander
    moveit_commander.roscpp_shutdown()


def move_bot_to_pose(pose,robot_group, robot_client):
    #set goal
    robot_group.set_named_target(pose)
    #plan using RRTConnect planner previously set
    _,robot_plan_home,_,_ = robot_group.plan()
    #create goal message
    robot_goal = moveit_msgs.msg.ExecuteTrajectoryGoal()
    #update the trajectory in the goal message 
    robot_goal.trajectory = robot_plan_home
    #sending goal to the action server
    robot_client.send_goal(robot_goal)
    robot_client.wait_for_result()
    return robot_client


if __name__=='__main__':
  try:
    simple_pick_place()
  except rospy.ROSInterruptException:
    pass