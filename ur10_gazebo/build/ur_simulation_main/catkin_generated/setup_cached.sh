#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/vaibhav/Workspace/ur10_gazebo/devel/.private/ur_simulation_main:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/vaibhav/Workspace/ur10_gazebo/devel/.private/ur_simulation_main/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/vaibhav/Workspace/ur10_gazebo/devel/.private/ur_simulation_main/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/vaibhav/Workspace/ur10_gazebo/build/ur_simulation_main'
export ROS_PACKAGE_PATH="/home/vaibhav/Workspace/ur10_gazebo/src/ur_simulation_main:$ROS_PACKAGE_PATH"